package link.luyu.protocol.link.fabric1;

public class LuyuDefault {
    public static final int GET_PROPERTIES = 22900;
    public static final int LIST_RESOURCES = 22901;
    public static final long ADAPTER_QUERY_EXPIRES = 30; // 30s
}
