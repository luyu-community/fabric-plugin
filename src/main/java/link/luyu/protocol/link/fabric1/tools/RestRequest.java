package link.luyu.protocol.link.fabric1.tools;

public class RestRequest<T> {
    public T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
