package main

import (
	"errors"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)


type CrossHelloWorld struct {
}

func (p *CrossHelloWorld) Init(stub shim.ChaincodeStubInterface) peer.Response {
	p.set(stub, "original")
	return shim.Success(nil)
}

func (p *CrossHelloWorld) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	var result string
	var err error
	if fn == "testSendTx" {
		result, err = p.testSendTx(stub, args)
	} else if fn == "testCall" {
		result, err = p.testCall(stub)
	} else if fn == "get" {
		result = p.get(stub)
	} else if fn == "set" {
		p.set(stub, args[0])
	} else if fn == "setCallback" {
		var nonce uint64
		nonce, _, err = ParseCallbackArgs(args)
		if err == nil {
			p.setCallback(stub, nonce)
		}
	} else if fn == "getCallback" {
		var nonce uint64
		var callbackArgs []string
		nonce, callbackArgs, err = ParseCallbackArgs(args)
		if err == nil {
			p.getCallback(stub, nonce, callbackArgs[0])
		}
	} else { // assume 'get' even if fn is nil
		err = errors.New(fmt.Sprintf("Unsupported method: %s", fn))
	}
	if err != nil {
		return shim.Error(err.Error())
	}

	// Return the result as success payload
	return shim.Success([]byte(result))
}

func (p CrossHelloWorld) testSendTx(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a value")
	}

	nonce, err := CrossSendTransaction(stub, "payment.bcos.HelloWorld", "set", []string{args[0]}, "0x222962196394e2e5ecc3fac11ab99b7446393ca1", "setCallback")

	if err != nil {
		return "", err
	}

	p.set(stub, "")

	return fmt.Sprintf("%d", nonce), nil
}

func (p CrossHelloWorld) setCallback(stub shim.ChaincodeStubInterface, nonce uint64) {
	p.set(stub, "callback called")
	p.testCall(stub)
}

func (p CrossHelloWorld) testCall(stub shim.ChaincodeStubInterface) (string, error) {
	nonce, err := CrossCall(stub, "payment.bcos.HelloWorld", "get", []string{}, "0x222962196394e2e5ecc3fac11ab99b7446393ca1", "getCallback")

	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d", nonce), nil
}

func (p CrossHelloWorld) getCallback(stub shim.ChaincodeStubInterface, nonce uint64, ret0 string) {
	p.set(stub, ret0)
}

func (p CrossHelloWorld) set(stub shim.ChaincodeStubInterface, arg string) {
	stub.PutState("test", []byte(arg))
}

func (p CrossHelloWorld) get(stub shim.ChaincodeStubInterface) string {

	value, _ := stub.GetState("test")

	return string(value)
}

// main function starts up the chaincode in the container during instantiate
func main() {
	if err := shim.Start(new(CrossHelloWorld)); err != nil {
		fmt.Printf("Error starting CrossHelloWorld chaincode: %s", err)
	}
}
